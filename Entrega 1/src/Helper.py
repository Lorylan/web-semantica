def normalize(string):
        aux = string.replace("."," ")
        aux = aux.strip()
        aux = aux.lower()
        aux = aux.replace("á","a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u")
        aux = aux.upper()    
        return aux

def merge(CLPData, CPData):
    updatedMovies = []
    CLPMovies = []
    CPMovies = []
    movieTitles = []
    for movie in CLPData:
        for movie2 in CPData:
            if(movie["TITULO"] == movie2["TITULO"] or samePeople(movie, movie2)):
                updatedMovies.append(update(movie, movie2))
                movieTitles.append(movie["TITULO"])
                movieTitles.append(movie2["TITULO"])
    CLPMovies = filterMovies(movieTitles, CLPData)
    CPMovies = filterMovies(movieTitles, CPData)
    finalData = {}
    finalData["MULTIPLES FUENTES"] = updatedMovies
    finalData["CINEMA LA PLATA"] = CLPMovies
    finalData["CINEPOLIS"] = CPMovies
    return finalData

    
def update(movie, movie2):
    result = {}
    shows = {}
    duplicatedData = intersection(movie.keys(), movie2.keys())
    movie1Data = difference(movie.keys(), movie2.keys())
    movie2Data = difference(movie2.keys(), movie.keys())
    for key in duplicatedData:
        if(key != "FUNCIONES"):
            if(len(movie[key]) > len(movie2[key])):
                result[key] = movie[key] 
            else:
                result[key] = movie2[key]  
        else:
            shows = {**movie[key], **movie2[key]}

    for key in movie1Data:
        result[key] = movie[key]
    
    for key in movie2Data:
        result[key] = movie2[key]
    
    result["FUNCIONES"] = shows

    return result
        
def samePeople(movie, movie2):
    if("DIRECTOR" in movie.keys() and "DIRECTOR" in movie2.keys()):
        directoresM1 = movie["DIRECTOR"].split(",")
        directoresM2 = movie2["DIRECTOR"].split(",")
        if(len(intersection(directoresM1, directoresM2))> 0):
            if("ACTORES" in movie.keys() and "ACTORES" in movie2.keys()):
                actoresM1 = movie["ACTORES"].split(",")
                actoresM2 = movie2["ACTORES"].split(",")
                return len(intersection(actoresM1, actoresM2)) > 2
    return False

def filterMovies(names, movies):
    result = []
    for movie in movies:
        if(not movie["TITULO"] in names):
            result.append(movie)
    return result
    
def intersection(lst1, lst2):
    return set(lst1).intersection(lst2)            

def difference(lst1, lst2):
    return set(lst1).difference(lst2)
