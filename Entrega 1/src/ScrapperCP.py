import bs4
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.options import Options 
from Helper import normalize

class CinepolisScrapper():

    def scrape(self):
        options = Options() 
        options.headless = True 
        driver = webdriver.Chrome(executable_path='C:/WebDriver/bin/chromedriver', chrome_options=options)
        driver.get("http://www.villagecines.com") #Abre en navegador en esta pagina
        
        moviesLink = []

        finalData = []

        for movie in driver.find_elements_by_xpath("//div[@class='featured-movies-grid-view-component movie-grid']/div/a[@class='movie-thumb d-flex flex-column lg']"):
            moviesLink.append(movie.get_attribute('href'))

        for movieLink in moviesLink:

            fichaTecnica = {}

            driver.get(movieLink)

            movieTitle = driver.find_element_by_xpath("//div[@class='container d-inline-flex justify-content-center align-items-center py-4']/h2").text
            fichaTecnica["TITULO"] = normalize(movieTitle)

            synopsis = driver.find_element_by_id('tabs-content').text
            fichaTecnica["SINOPSIS"] = normalize(synopsis)

            driver.find_element_by_id('tecnicos-tab').click()#hago el click para acceder a los datos tecnicos

            wait = WebDriverWait(driver, 10).until(EC.text_to_be_present_in_element((By.ID,"tecnicos"),"Título"))

            movieData = driver.find_element_by_id('tecnicos').text
            datos = movieData.split('\n')

            for dato in datos:
                aux = dato.split(':')
                fichaTecnica[normalize(aux[0])] = normalize(aux[1])

            shows = {}

            for cinema in driver.find_elements_by_xpath("//div[@class='card panel panel-primary']"):
                cinema.click()
                time.sleep(1.5)
                schedules = []
                for tupla in cinema.find_elements_by_class_name("movie-showtimes-component"):
                    times = cinema.find_elements_by_class_name("row")
                    even = False
                    showData = {}
                    for horario in times:       
                        if(even):
                            showData["HORARIO"] = normalize(horario.text).replace("\n"," - ")
                            even = False
                            schedules.append(showData)
                            showData = {}
                        else:
                            showData["SALA"] = normalize(horario.text)
                            even = True

                shows[normalize(cinema.text.split("\n")[0])] = schedules
                        
            fichaTecnica["FUNCIONES"] = shows

            finalData.append(fichaTecnica)
        
        
        driver.close()
        return finalData