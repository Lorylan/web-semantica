import bs4
import requests
import json
from Helper import normalize


class CinemaLaPlataScrapper():

    def scrape(self):
        url = "http://www.cinemalaplata.com/Cartelera.aspx"
        results = requests.get(url).text
        soup = bs4.BeautifulSoup(results,"html.parser")

        finalData = []

        movies = soup.find_all('div', attrs={"class":"page-container singlepost"})

        for movie in movies:

            link = movie.find('a').get('href')
            fullLink = "http://www.cinemalaplata.com/"+link
            movieHTML = requests.get(fullLink).text

            movieSoup = bs4.BeautifulSoup(movieHTML,"html.parser")

            movieTitle = movieSoup.find('div', attrs={"class":"post-container page-title"}).text.strip()
            movieData = movieSoup.find_all('div', attrs={"class":"dropcap6"}) 

            movieSynopsis = movieSoup.find('div', attrs={"class":"message-box-1"}).text.strip()

            movieInfo = {}

            movieInfo["TITULO"] = normalize(movieTitle)

            for datos in movieData:
                tipoDato = datos.find('h4').text.strip()
                if(tipoDato.upper() != "IDIOMA"):     
                    dato = datos.find('p').text.strip()
                    movieInfo[normalize(tipoDato)] = normalize(dato)
            
            movieInfo["SINOPSIS"] = normalize(movieSynopsis)

            movieShows = movieSoup.find_all('div', attrs={"class":"col-2"})
            
            shows = {}
            cinema = ""

            for show in movieShows:
                rooms = []
                cinema = show.find('h5').text.strip()
                showTimes = show.find('p').find_all('span')
                for time in showTimes:
                    hs = time.text.split(":",1)
                    hs = normalize(hs[0]) + " : " + normalize(hs[1])
                    rooms.append(hs)
                aux = cinema.split("-")
                shows[normalize(aux[0])] = {"SALA":normalize(aux[1]) , "HORARIOS":rooms}

            movieInfo["FUNCIONES"] = shows

            finalData.append(movieInfo)

        return finalData
    
    