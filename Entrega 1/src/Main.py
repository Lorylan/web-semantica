from ScrapperCLP import CinemaLaPlataScrapper
from ScrapperCP import CinepolisScrapper
import json
from Helper import merge
import os

CLP = CinemaLaPlataScrapper()
CP = CinepolisScrapper()

print("Comenzando scrapping de Cinema La Plata")
CLPData = CLP.scrape()
print("Comenzando scrapping de Cinepolis")
CPData = CP.scrape()
print("Comenzado mergeo y guardado de datos")
finalData = merge(CLPData, CPData)

rutaActual = os.path.dirname(os.path.abspath(__file__))
ruta = os.path.join(rutaActual,"..","data","result.json")
with open(ruta, 'w', encoding='utf-8') as fp:
        json.dump(finalData, fp, ensure_ascii=False, indent=4)

print("Programa Finalizado.")