a) Obtener a los escritores que hayan nacido en una ciudad de Argentina.
 
https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20DISTINCT%20%3FPerson%20%0Awhere%20%7B%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20dbr%3AArgentina%7D%0AUNION%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%3Flocation.%20%3Flocation%20dbo%3Acountry%20dbr%3AArgentina%7D%0A%7D%0A&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on

b) Obtener a los escritores que hayan nacido en una ciudad de Uruguay.

https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20DISTINCT%20%3FPerson%20%0Awhere%20%7B%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20dbr%3AUruguay%7D%0AUNION%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%3Flocation.%20%3Flocation%20dbo%3Acountry%20dbr%3AUruguay%7D%0A%7D%0A&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on

c) Utilizando el keyword filter (vea sección 6.3.2.6 del libro), obtener a los escritores que hayan nacido en una ciudad de Argentina o de Uruguay 

https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20%3FPerson%20%0Awhere%20%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%20%3FbirthPlace%0AFilter%20(%20%3FbirthPlace%20%3D%20dbr%3AUruguay%20%7C%7C%20%20%3FbirthPlace%20%3D%20dbr%3AArgentina)%0A%7D%0A&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on


d) Utilizando el keyword union (vea sección 6.3.2.6 del libro), obtener a los escritores que hayan nacido en una ciudad de Argentina o de Uruguay

https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20%3FPerson%20%0Awhere%20%7B%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%20dbr%3AUruguay%20%7D%0AUNION%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%20dbr%3AArgentina%7D%0A%7D&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on


Nota:
Cuando buscamos un escritor que haya nacido en Argentina o Uruguay notamos que hay algunas personas que en la propiedad dbo:birthPlace hay una ciudad o ciudad y provincia en vez de un país. Para tener en cuenta este dato en la consulta tuvimos que usar tanto filter como union, por lo que no lo pudimos poner ni en la respuesta c ni d. 
https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20DISTINCT%20%3FPerson%20%0Awhere%20%7B%20%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%3Flocation.%20%3Flocation%20dbo%3Acountry%20%3FData.%0AFILTER%20(%3FData%3D%20%20dbr%3AUruguay%20%7C%7C%20%3FData%3D%20dbr%3AArgentina)%7D%0AUNION%0A%7B%3FPerson%20dbo%3Aoccupation%20dbr%3AWriter.%20%3FPerson%20dbo%3AbirthPlace%20%3FLocation.%0AFILTER%20(%3FLocation%20%3D%20%20dbr%3AUruguay%20%7C%7C%20%3FLocation%20%3D%20dbr%3AArgentina)%7D%0A%7D%0A&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on


e) ¿Qué diferencia hay entre c y d? ¿En cual se deben recuperar/analizar menor número de tripletas?
Usando la opcion de filter, lo que hacer es: Por cada persona donde su ocupaccion sea Escritor, verifica/chequea que haya nacido en Uruguay o en Argentia, si algunas de estas condiciones se considera verdadera entonces se agrega al resultado.
Por otro lado, si usamos la opcion de union, se realizan dos consultas por separado. Por un lado busca las personas donde su ocupacion sea Escritor y su lugar de nacimiento sea Uruguay. Luego se hace otra consulta donde su ocupacion sea Escritor y su lugar de nacimiento sea Argentina.Por último devolvera la union de ambas consultas.
Por la explicacion anterior creemos que debemos recuperar/analizar menor número de tripletas cuando utilizamos un filter, esto se debe a que si realizamos una consulta con UNION analizamos dos veces las tripletas, una para cada consulta. 

f) ¿Cuantos empleados tiene la compañía mas grande en dbpedia, y en que país está ubicada? (obtenga la lista de todas las compañías y los países donde están ubicadas ordenada de forma descendiente por numero de empleados)

https://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&qtxt=select%20%3FCompany%20%20%3FCountry%20%3FEmployeed%0Awhere%20%7B%0A%7B%20%3FCompany%20rdf%3Atype%20dbo%3ACompany.%20%0A%3FCompany%20dbo%3AnumberOfEmployees%20%3FEmployeed%20.%0A%3FCompany%20dbo%3Alocation%7Cdbp%3AhqLocationCountry%7Cdbp%3AlocationCountry%20%3FCountry%7D%0A%7DORDER%20BY%20DESC(%3FEmployeed)%0ALIMIT%201&format=text%2Fhtml&timeout=30000&signal_void=on&signal_unconnected=on

Mientras realizamos la consulta encontramos que para saber la cantidad de empleado se utiliza tambien la propiedad dbp:NumEmployees. No la consideramos porque es menos confiable que dbo, y algunas tripletas usando dicha propiedad nos devolvian fechas.
