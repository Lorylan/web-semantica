import os
import Scrapper
import json
import Merge
import Helper
import OWL_Maker

print("Comenzando Scrapping...")
movieInformation = Scrapper.scrape()

print("Normalizando datos...")
movieInformation = Helper.normalize(movieInformation)
          
print("Mergeando datos...")
datosMergeados = Merge.merge(movieInformation)

print("Tomando datos de salas y cines...")
rutaActual = os.path.dirname(os.path.abspath(__file__))
ruta = os.path.join(rutaActual,"..","data","CinemasData.json")
with open(ruta, 'r', encoding='utf-8') as fp:
        cinemaData = json.load(fp)
        
print("Transformando datos a OWL...")
finalData = OWL_Maker.MakeOwl(datosMergeados,cinemaData)

print("Fin del proceso.")