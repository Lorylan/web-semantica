def normalize(movies):

    for movie in movies:
        if "actors" in movie.keys():
                movie["actor"] = movie.pop("actors")
        if "hasPart" in movie.keys():
                movie.pop("hasPart")
        if "creator" in movie.keys():
            movie["creator"].pop(0)
            movie["creator"].pop(0)
    movies[0] = fixURL(movies[0])
    return movies

def fixURL(data):
    # Arregla la url de imdb
    if isinstance(data, dict):
        for key in data.keys():
            data[key] = fixURL(data[key])
    elif isinstance(data, list):
        for item in data:
            data[data.index(item)] = fixURL(item)
    else:
        if isinstance(data, str) and data[:1] == "/":
            data = "www.imdb.com" + data
    return data