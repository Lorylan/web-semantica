import bs4
import requests
import json
import os

def scrape():
    rutaActual = os.path.dirname(os.path.abspath(__file__))
    with open(rutaActual + "/DataSources.txt","r") as sources:
        pages = sources.readlines()

    movies = []
    for page in pages: 
        page = page.replace("\n", "")
        pageName = page.split(".")[1]
        headers = {
            "User-Agent" : "Googlebot/2.1 (+http://www.google.com/bot.html)"
        }
        pageText = requests.get(page, headers=headers).text
        soup = bs4.BeautifulSoup(pageText, "html.parser")

        JLD = soup.find('script', attrs={"type":"application/ld+json"}).string
        movieData = json.loads(JLD, strict=False)

        movies.append(movieData)
            
        print("Scrapping completo en " + page)

    print("Scrapping de todas las paginas terminado.")

    return(movies)



