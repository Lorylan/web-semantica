from rdflib.namespace import OWL, RDF
from rdflib import Namespace, Graph, BNode, Literal, URIRef
import json
import os

MovieModelOnt = Namespace("https://gitlab.com/Lorylan/web-semantica/-/raw/master/Entrega%203/Movie_model.ttl#")
schemaOrg = Namespace("https://schema.org/")
counters = {}
g = Graph()
g.namespace_manager.bind('', MovieModelOnt)

def MakeOwl(movieData,cinemaData):

    #Obtengo el nombre de la pelicula
    if 'name' in movieData.keys():
        if isinstance(movieData['name'],list):
            movieName = movieData['name'].first()
        else:
            movieName = movieData['name']
    elif 'title' in movieData.keys():
        if isinstance(movieData['title'],list):
            movieName = movieData['title'].first()
        else:
            movieName = movieData['title']
    movieName = movieName.strip().replace(" ", "_")

    # Agarra los datos del JSON_LD y los carga en el grafo
    convert(movieData,movieName)

    # Agarra los datos del TP1 y los carga en el grafo
    integrate(cinemaData, movieName)

    #Se escribe el archivo
    rutaActual = os.path.dirname(os.path.abspath(__file__))
    ruta = os.path.join(rutaActual,"..","data","Movie_Model.ttl")
    archivo = open(ruta,'w')
    archivo.write(g.serialize(format="turtle").decode("utf-8"))
    archivo.close()

def convert(data,movieName):
    element = None
    if isinstance(data, dict):
        data.pop('@context', None)

        if 'name' in data.keys() and '@type' in data.keys() and (data["@type"] == "Person" or data["@type"] == "Organization" or data["@type"] == "Movie"):
            name = data['name'].strip().replace(" ","_")
            element = MovieModelOnt[name]
        else:
            element = MovieModelOnt[buildName(data,movieName)]
        g.add((element, RDF.type, OWL.NamedIndividual))

        tipo = data.pop('@type', None)
        g.add((element, RDF.type, schemaOrg[tipo]))

        for prop, info in data.items():
            schemaprop = schemaOrg[prop]
            if isinstance(info,list):
                for each in info:
                    g.add((element, schemaprop, convert(each,movieName)))
            else:
                g.add((element, schemaprop, convert(info,movieName)))
    else:
        element = Literal(data)
    return element

def buildName(data,movieName):
    # Armamos nombres genericos
    number = 1
    if "@type" in data.keys():
        if data["@type"] in counters.keys():
            number = counters[data["@type"]]
            counters[data["@type"]] = number+1
        else:
            counters[data["@type"]] = 2
    
        new_name = movieName + "_" + data["@type"] + "_" + str(number)
        return new_name.strip().replace(" ","_")

def integrate(data, movieName):
    movie = None
    screeningCounter = 1
    for d in data["MULTIPLES FUENTES"]:
        if d["TITULO"] == "MUJER MARAVILLA 1984":
            movie = d
    for cinema_name, value in movie["FUNCIONES"].items():
        # Carga el cine
        name = cinema_name.replace(" ","_")
        cinema = MovieModelOnt[name]
        g.add((cinema, RDF.type, OWL.NamedIndividual))
        g.add((cinema, RDF.type, schemaOrg["MovieTheater"]))


        if isinstance(value, dict):
            room_id = value["SALA"].strip().replace(" ","_")
            roomName = Literal(value["SALA"])
            schedule = Literal(value["HORARIOS"])
        else:
            room_id = value[0]["SALA"].strip().replace(" ","_")
            roomName = Literal(value[0]["SALA"])
            schedule = Literal(value[0]["HORARIO"])

        #Carga la sala
        roomFullName = name + "_" + room_id + "_1"
        room = MovieModelOnt[roomFullName]
        g.add((room, RDF.type, OWL.NamedIndividual))
        g.add((room, RDF.type, schemaOrg["Room"]))
        g.add((cinema, schemaOrg["containsPlace"], room))
        g.add((cinema, MovieModelOnt["name"],Literal(name)))
        g.add((room, MovieModelOnt["name"], roomName))  
        
        #Carga la funcion
        screening = MovieModelOnt[movieName + "_ScreeningEvent_" + str(screeningCounter)]
        screeningCounter += 1
        g.add((screening, RDF.type, OWL.NamedIndividual))
        g.add((screening, RDF.type, schemaOrg["ScreeningEvent"]))
        g.add((screening, schemaOrg["location"], room))
        g.add((screening, MovieModelOnt["scheduledTime"], schedule))
        g.add((screening, schemaOrg["workPresented"], MovieModelOnt[movieName]))






