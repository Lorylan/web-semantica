def merge(jsons):
    result = jsons[0]
    for i in jsons:
        if(i != result):
            result = dictmerge(result,i)
    return result

def dictmerge(d1, d2):
    if(d1 == d2):
        return d1
    if("@type" in d1.keys() and "@type" in d2.keys()):
        if(d1["@type"] == d2["@type"]):
            if(d1["@type"] == "Person"):
                if(d1["name"] == d2["name"]):
                    for j in d2.keys():
                        if(j not in d1.keys()):
                            d1[j] = d2[j] 
                        elif(d1[j] != d2[j]):
                            d1[j] = [d1[j], d2[j]]
                    return d1
                else:
                    return [d1, d2]
            elif(d1["@type"] == "AggregateRating" or d1["@type"] == "Review" or d1["@type"] == "VideoObject"):
                return [d1,d2]
    result = d1.copy()
    result = keyCompare(result, d2)
    return result

def keyCompare(result, dicc):
    for key in dicc.keys():
        if(key not in result.keys()):
            result[key] = dicc[key]
        else:
            aux = processdicts(result, dicc, key)
            if(isinstance(aux, list) and isinstance(result[key], list)):
                result[key] = listmerge(aux, result[key])
            else:
                result[key] = aux
    return result
        
def processdicts(item1, item2, key):
    if(isinstance(item1[key],dict) and isinstance(item2[key],dict)):
        return dictmerge(item1[key], item2[key])
    elif(isinstance(item2[key],list) and isinstance(item1[key],list)):
        return listmerge(item1[key], item2[key])
    elif(isinstance(item2[key],list) and not isinstance(item1[key], list)):
        return listadd(item2[key], item1[key])
    elif(not isinstance(item2[key], list) and isinstance(item1[key], list)):   
        return listadd(item1[key], item2[key])
    elif(not isinstance(item2[key], list) and not isinstance(item1[key], list)):
        return commonadd(item1[key], item2[key])

def listmerge(l1, l2):
    result = []
    for item in l2:
        result = listadd(l1, item)
    return result

def listadd(li, it):
    if(it in li):
        return li
    if(isinstance(it, dict)):
        added = False
        for item in li:
            if(isinstance(item, dict)):
                result = dictmerge(item,it)
                if(not isinstance(result, list)):
                    li[li.index(item)] = result
                    added = True
                    break
        if not added:
            li.append(it)
    else:
        li.append(it)
    return li
            
def commonadd(item1, item2):
    if(isinstance(item1,dict)):
        if(item2 in item1.values()):
            return item1
        return [item1, item2]
    if(isinstance(item2, dict)):
        if(item1 in item2.values()):
            return item2
        return [item2, item1]
    if(item1 == item2):
        return item1
    return [item1, item2]