import requests
import bs4
import json
import os

pageHTMLMovie = requests.get("https://www.rottentomatoes.com/m/wonder_woman_1984").text
soup = bs4.BeautifulSoup(pageHTMLMovie,"html.parser")
datamovie = soup.find('script', attrs={"type":"application/ld+json"}).string
jsonData = json.loads(datamovie)
datosPelis = {}
datosPelis['Nombre'] = jsonData['name']
datosPelis['Clasificacion'] = jsonData['contentRating']
datosPelis['Genero'] = jsonData['genre']
datosPelis['Autor'] = jsonData['author']
datosPelis['Director'] = jsonData['director']
datosPelis['Reseña'] = jsonData['review']
datosPelis['Actores'] = jsonData['actors']
datosPelis['Personajes'] =  jsonData['character']
datosPelis['Compania productora'] = jsonData['productionCompany']
datosPelis['Puntaje'] = jsonData['aggregateRating']

rutaActual = os.path.dirname(os.path.abspath(__file__))
ruta = os.path.join(rutaActual,"..","data","result.json")
with open(ruta, 'w', encoding='utf-8') as fp:
        json.dump(datosPelis, fp, ensure_ascii=False, indent=4)
