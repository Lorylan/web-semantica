import requests
import bs4
import json
import sys
import os
from rdflib import Graph
from rdflib.namespace import OWL
from urllib.request import Request, urlopen
from urllib.parse import quote
import rdflib
import string

def get_nombre(url):
    return url.rsplit("/",1)[1]
def get_url(url):
    return url.rsplit("/",1)[0]

if __name__ == "__main__":
    if (len(sys.argv) == 3):
        g = Graph()
        g.parse(sys.argv[2], format="turtle", encoding="utf-8")
        DBO = rdflib.Namespace('http://dbpedia.org/ontology/')
        PROV =	rdflib.Namespace('http://www.w3.org/ns/prov#')
        result = Graph()
        result.parse(sys.argv[1], format="turtle", encoding="utf-8")
        result.parse(sys.argv[2], format="turtle", encoding="utf-8")
        for person, prop, data in g.triples((None,  OWL.sameAs, None)):
            page = Request(data,headers={"Accept":"text/turtle"})
            nombre = get_nombre(page.full_url)
            url = get_url(page.full_url)+"/"
            page.full_url = f'{url}{quote(nombre)}'
            response = urlopen(page)
            aux = Graph()
            aux.parse(response.url,format="turtle", encoding="utf-8")
            result += aux.triples((None, DBO.birthDate, None))
            result += aux.triples((None, DBO.occupation, None))
            result += aux.triples((None, PROV.wasDerivedFrom, None))   
        print(result.serialize(format="turtle").decode("utf-8"))
    else:
       print("Error en los parametros, se necesitan 2\n Un ttl base y uno para acoplar datos.")